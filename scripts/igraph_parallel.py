#! /usr/bin/python
# If you have any problems with the script, please contact
# Naren C S <narenschandran@gmail.com>
import os
import sys
import argparse
import subprocess
import igraph
import math
import heapq
import multiprocessing as mp
from statsmodels.stats.multitest import multipletests
from scipy.stats import rankdata, hypergeom

def make_str(l):
    return [str(k) for k in l]

def calculate_pval(tup):
    x = tup[0]
    M = tup[1]
    n = tup[2]
    N = tup[3]
    return hypergeom.sf(x, M, n, N)


def spath_iterator(con):
    for l in con:
        lsplit = l.strip().split("\t")
        yield lsplit


def get_outstr(source_id):
    path_costs = G.shortest_paths_dijkstra(source=source_id, weights="weight", mode="OUT")[0]
    paths = G.get_shortest_paths(source_id, weights="weight", mode="OUT")
    outstrs = []
    for i in range(len(path_costs)):
        path_cost = path_costs[i]
        if path_cost!=0 and path_cost!=finf:
            path = paths[i]
            path_length=len(path) - 1
            if path_length>=min_path_length:
                npath_cost = path_cost/path_length
                out_nodes = [nmap[node] for node in path]
                outstr = "\t".join([out_nodes[0], out_nodes[-1]] + [str(node) for node in (path_cost, npath_cost, path_length)] + [",".join(out_nodes), "\n"])
                #outstr = [out_nodes[0], out_nodes[-1]] + [str(node) for node in (path_cost, npath_cost, path_length)] + [",".join(out_nodes)]
                outstrs.append(outstr)
    if len(outstrs)>0:
        return "".join(outstrs)


if __name__=="__main__":

    parser=argparse.ArgumentParser()
    parser.add_argument("filename", help="The input file to be processed")
    parser.add_argument("-m", "--min_path_length", help="All paths which are lesser than this length are discarded", type=int, default=2)
    parser.add_argument("-n", "--max_top_nodes", type=int, help="Max nodes after which path exploration is stopped", default=2000)
    parser.add_argument("-e", "--max_top_edges", type=int, help="Max edges after which path exploration is stopped. This check will happen after node size check, so set max_top_nodes to an arbitrarily high value (at least as large as total number of nodes) if you plan on using this option. ", default=float('inf'))
    parser.add_argument("-o", "--output_path", help="Folder in which the output will be stored", default=None)
    parser.add_argument("-p", "--threads", help="Number of threads to be used for processing", default=max(1, mp.cpu_count()-2))
    parser.add_argument("-c", "--percentile", help="Percentile sorted paths to be outputted", default=0.05, type=float)
    parser.add_argument("-d", "--deg_file", help="DEG file for doing hypergeometric", default=None)
    parser.add_argument("--separate_dir", help="Create separate sub-directory for the igraph results", action="store_true")

    args            = parser.parse_args()
    filename        = args.filename
    bname           = os.path.splitext(os.path.basename(filename))[0]
    min_path_length = args.min_path_length
    pthreads        = int(args.threads)
    separate_dir    = args.separate_dir
    percentile      = args.percentile
    output_path     = args.output_path
    deg_file        = args.deg_file
    max_top_nodes   = args.max_top_nodes
    max_top_edges   = args.max_top_edges

    #filename="./analysis/lung_cancer/GSE20189_GPL571/GSE20189_GPL571_blood_lung_cancer_IIB/GSE20189_GPL571_blood_lung_cancer_IIB_DOWN_EDGES.tsv"
    #max_top_nodes=2000
    #output_path=None
    #pthreads=8
    #separate_dir=False
    #deg_file="./analysis/lung_cancer/GSE20189_GPL571/GSE20189_GPL571_blood_lung_cancer_IIB/GSE20189_GPL571_blood_lung_cancer_IIB_DOWN_DEGS.txt"
    if output_path == None:
        output_path = os.path.dirname(filename)
    output_path = os.path.realpath(output_path)
    hd = False # HIDDEN OPTION. Set to True if you want header line
    
    if separate_dir:
        output_folder_name = bname + "_IGRAPH_OUTPUT"
        output_folder = os.path.join(output_path, output_folder_name)
    else:
        output_folder = output_path
        
    if not os.path.exists(output_folder):
        os.makedirs(output_folder)

    print(output_folder)

    fname           = bname + "_IGRAPH_SPATH_OUTPUT.tsv"
    foutname        = os.path.join(output_folder, fname)
    sorted_fname    = bname + "_SORTED_SPATH_OUTPUT.tsv"
    sorted_foutname = os.path.join(output_folder, sorted_fname)
    print(foutname)

    header_line="NodeA\tNodeB\tpath_score\tnormalized_path_score\tpath_length\tpath_trace\n"

    print("Processing file: " + filename)
    print("Using minimum path length of: " + str(min_path_length))
    print("Outputting to: " + foutname)
    print("Output header line: " + str(hd))

    if hd:
        print "Header line is:", header_line
    elist = {(l[0], l[1]):float(l[2]) for line in open(filename) for l in [line.strip().split("\t")]}
    rank_elist = dict(zip(elist.keys(), rankdata([i for i in elist.values()], method="min")))
    G = igraph.Graph.Read_Ncol(filename, names=True, weights=True, directed=True)
    all_nodes = range(len(G.vs))
    nnodes = len(G.vs)
    nedges = len(G.es)
    nmap = {node:G.vs[node]["name"] for node in range(len(G.vs))}
    finf = float("inf")
    output=open(foutname, "w")
    if hd:
        output.write(header_line)
    print "Calculating shortest paths from all nodes to all nodes"
    print "Using", pthreads, "threads"
    chunk_size = 2000
    chunks = [all_nodes[n:n+chunk_size] for n in range(0, len(all_nodes), chunk_size)]
    p=mp.Pool(pthreads)
    for nlist in chunks:
        a = p.map(get_outstr, nlist)
        b = filter(None, a) # Removes empty strings, if present
        if len(b)>0:
            output.write("".join(b))
# if pthreads > 1:
    p.close()
    output.close()

    wcpipe = subprocess.Popen(["wc", "-l", foutname], stdout=subprocess.PIPE)
    wcstr = wcpipe.communicate()[0]

    lc = int(wcstr.strip().split(" ")[0])
    print(lc)
    nlines = int(math.ceil(lc*(percentile/100.0)))
    sorted_lines = heapq.nsmallest(nlines, spath_iterator(open(foutname, "r")), lambda x : float(x[3]))

    all_edges = []
    all_nodes = []
    out_lines = []
    hypergeom_outlines = []
    if deg_file != None:
        degs = set([deg.strip() for deg in open(deg_file, "r")])
    else:
        degs = []

    if len(degs)>0:
        M = nnodes
        n = len(degs)
        network_nodes = set(nmap.values())
        hypergeom_list = []
    
    for line in sorted_lines:
        line_nodes = line[5].split(",")
        line_edges = [(line_nodes[i], line_nodes[i+1]) for i in range(len(line_nodes)-1)]
        new_edges = [edge for edge in line_edges if edge not in all_edges]
        new_nodes = [node for node in line_nodes if node not in all_nodes]
        all_edges = all_edges + new_edges
        all_nodes = all_nodes + new_nodes
        new_nodes_added = len(new_nodes)
        new_edges_added = len(new_edges)
        sort_ew = sorted([("_".join(edge), elist[edge], rank_elist[edge], rank_elist[edge]/float(nedges)) for edge in line_edges], key = lambda x: x[1])
        ew_out = list(sort_ew[0]) + list(sort_ew[-1])
        if len(degs)>0:
            N = len(all_nodes)
            intersect = set(all_nodes).intersection(degs)
            x = max(len(intersect) - 1, 0)
            sprop = x/float(N)
            pprop = n/float(M)
            dprop = x/float(n)
            enr   = sprop/pprop
            hypergeom_list.append([x, M, n, N, sprop, pprop, dprop, enr])
#            pval = hypergeom.sf(x, M, n, N)
        out_line = line + [len(new_nodes), len(all_nodes), len(new_edges), len(all_edges)] + ew_out
        out_line = "\t".join([str(el) for el in out_line])
        out_lines.append(out_line)
        if len(all_nodes)>=max_top_nodes:
            break
        if len(all_edges)>=max_top_edges:
            break
    if len(degs)>0:
        p = mp.Pool(pthreads)
        pvals = p.map(calculate_pval, hypergeom_list)
        p.close()
        mtest_out = multipletests(pvals, method="fdr_bh", alpha=0.05)
        sig = list(mtest_out[0])
        qvals = list(mtest_out[1])
        hypergeom_list_out = [make_str(l) for l in hypergeom_list]
        olist = ["\t".join([x[0]] + x[1] + [str(x[2]), str(x[3]), str(x[4])]) for x in zip(out_lines, hypergeom_list_out, pvals, qvals, sig)]
    else:
        na_line = "\t" + "NA\t"*10 + "NA" 
        olist = [line + na_line for line in out_lines]

    sorted_output = open(sorted_foutname, "w")
    out_header = "nodeA\tnodeB\tpath_cost\tnormalized_path_cost\tpath_length\tpath_trace\tnew_nodes\ttotal_top_nodes\tnew_edges\ttotal_top_edges\tbest_edge\tbest_edge_wt\tbest_edge_rank\tbest_edge_quantile\tworst_edge\tworst_edge_weight\tworst_edge_rank\tworst_edge_quantile\tdegs_in_top\tnodes_in_base\ttotal_degs\ttop_nodes\ttop_net_deg_prop\tbase_net_deg_prop\tdeg_prop\tdeg_enrich\tpval\tqval\tsignificant\n"
    sorted_output.write(out_header)
    sorted_output.write("\n".join(olist))
    sorted_output.close()
    subprocess.call(["rm", foutname])
