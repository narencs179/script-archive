Installing `python-igraph`, `statsmodels` and `argparse` should pull in all other required dependencies. The required dependencies are stored in `dependencies/igraph_parallel.txt`. As of now, the code is Python 2 only. As a consequence of this, versions of statsmodels >= 0.11 and python-igraph >= 0.8.3 do not work. This is automatically handled if the `dependencies/igraph_parallel.txt` file is used to install the dependencies as follows:

`pip2 install -r dependencies/igraph_parallel.txt`

The script has a lot of options, which can be checked by typing:

`python2 igraph_parallel.py -h`

A typical run would be invoked by the command:

`python2 igraph_parallel.py <weighted_edge_file>`

- Format for `<weighted_edge_file>`:
    - 3 columns of data: NodeA, NodeB, Edge weight
    - No header line
    - Entries in each row should be separated by tabs
